import Foundation

public protocol NetworkDispatcher {
    func dispatch(request: Request,
                  onSuccess: @escaping (Data) -> Void,
                  onError: @escaping (Error) -> Void)
}

public enum RequestError: Swift.Error {
    case invalidURL
    case noData
    case error
}

public struct URLSessionNetworkDispatcher: NetworkDispatcher {
    public static let instance = URLSessionNetworkDispatcher()
    private init() { }
    
    public func dispatch(request: Request, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
        guard let url = URL(string: request.path) else {
            onError(RequestError.invalidURL)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        
        do {
            if let params = request.params {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            }
        } catch let error {
            onError(error)
            return
        }
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                onError(error)
                return
            }
            
            guard let _data = data else {
                onError(RequestError.noData)
                return
            }
            
            onSuccess(_data)
            }.resume()
        
    }
}
