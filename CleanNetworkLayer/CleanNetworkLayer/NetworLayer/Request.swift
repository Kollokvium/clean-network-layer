import Foundation
import UIKit
import Network

public protocol RequestType {
    associatedtype ResponseType: Codable
    var data: Request { get }
}

public struct Request {
    public enum Method: String {
        case post = "POST"
        case put = "PUT"
        case get = "GET"
        case delete = "DELETE"
    }
    
    public let path: String
    public let method: Method
    public let params: [String: Any?]?
    public let headers: [String: String]?
}

