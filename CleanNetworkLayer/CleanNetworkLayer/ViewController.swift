import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        newOrcBirth()
    }
    
    fileprivate func newOrcBirth() {
        OrcBirthDomainService().execute(onSuccess: { (orc: Orc) in
            print("Give to \(orc) some weapon")
        }, onError: { (error: Error) in
            print("Orc have some \(error)")
        })
    }
}
