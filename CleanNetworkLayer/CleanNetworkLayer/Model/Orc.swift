import Foundation

struct Orc: Codable {
    let weapon: String
    let skinColor: String
}
